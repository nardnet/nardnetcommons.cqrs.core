package com.nardnetcommons.cqrs.core.queries;

import java.util.List;

import com.nardnetcommons.cqrs.core.domain.BaseEntity;

@FunctionalInterface
public interface QueryHandlerMethod<T extends BaseQuery> {
	List<BaseEntity> handle(T query);
}
