package com.nardnetcommons.cqrs.core.queries;

import java.util.List;

import com.nardnetcommons.cqrs.core.domain.BaseEntity;

public interface QueryDispatcher {
	<T extends BaseQuery> void registerHandler(Class<T> type, QueryHandlerMethod<T> handler);
	<U extends BaseEntity> List<U> send(BaseQuery query);
}
