package com.nardnetcommons.cqrs.core.infrastructure;

import com.nardnetcommons.cqrs.core.commands.BaseCommand;
import com.nardnetcommons.cqrs.core.commands.CommandHandlerMethod;

public interface CommandDispatcher {
	<T extends BaseCommand> void registerHandler(Class<T> type, CommandHandlerMethod<T> handler);
	void send(BaseCommand command);
}
