package com.nardnetcommons.cqrs.core.handlers;

import com.nardnetcommons.cqrs.core.domain.AggregateRoot;

public interface EventSourcingHandler<T> {
	
	void save(AggregateRoot aggregate);
	T getById(String id);
	void republishEvents();

}
