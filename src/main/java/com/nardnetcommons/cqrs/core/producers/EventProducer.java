package com.nardnetcommons.cqrs.core.producers;

import com.nardnetcommons.cqrs.core.events.BaseEvent;

public interface EventProducer {
	
	void produce(String topic, BaseEvent event);

}
